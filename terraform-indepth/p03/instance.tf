resource "aws_instance" "jaks-inst" {
  ami = var.ami_image
  instance_type = var.instance-type
  availability_zone = data.aws_availability_zones.azets.names[0]
  security_groups = [aws_security_group.jakas_tam_groupa.name]

  tags = {
    Name = "mein-instance"
  }
}

resource "aws_security_group" "jakas_tam_groupa" {
  name = "probna-sq"

  egress {
    from_port = 8080
    protocol = "tcp"
    to_port = 8080
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "jakas-tam-groupa"
    burak = "to ja!"
  }
}