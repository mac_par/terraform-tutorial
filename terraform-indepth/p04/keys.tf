resource "aws_key_pair" "main_key" {
  public_key = file(var.key_path)
  key_name = "main_key_pair"
}