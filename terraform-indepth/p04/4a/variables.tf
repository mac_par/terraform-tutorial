variable "region" {
  default = "eu-west-1"
  type = string
}

variable "instance-type" {
  default = "t2.micro"
  type = string
}

variable "ami_image" {
  default = "ami-0ea3405d2d2522162"
  type = string
}

variable "key_path" {
  type = string
  default = "keys/mykey.pub"
}

variable "bash_script_path" {
  type = string
  default = "user-data.sh"
}