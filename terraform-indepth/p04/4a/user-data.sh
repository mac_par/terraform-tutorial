#!/usr/bin/env bash
sudo su
yum upgrade -y
yum install -y httpd.x86_64
systemctl start httpd.service
systemctl enable httpd.service
hostname -f > /var/www/html/index.html