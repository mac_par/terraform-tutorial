resource "aws_key_pair" "instance-key" {
  public_key = file(var.key_path)
  key_name = "instance-access-key"
}