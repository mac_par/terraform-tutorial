data "template_file" "data_user_script" {
  template = file(var.bash_script_path)
}

resource "aws_instance" "main-instance" {
  ami = var.ami_image
  instance_type = var.instance-type
  security_groups = [aws_security_group.allow-ssh.name, aws_security_group.allow-web.name]
  key_name = aws_key_pair.instance-key.key_name
  user_data = data.template_file.data_user_script.rendered
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group" "allow-ssh" {
  name = "allow-ssh-sg"
  description = "Allows connection via ssh port"
  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow-web" {
  name = "allow-web-sg"
  description = "Allows connection via port 80"
  ingress {
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
}