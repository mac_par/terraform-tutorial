resource "aws_instance" "probna-instancja" {
  ami = var.ami_image
  instance_type = var.instance-type
  availability_zone = data.aws_availability_zones.azets.names[0]
  security_groups = [aws_security_group.ssh-group.name]
  key_name = aws_key_pair.main_key.key_name

  tags = {
    Name = "probna-instancja"
  }
}

resource "aws_security_group" "ssh-group" {
  name = "ssh-security-group"
  description = "Allows access via ssh"

  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow-ssh-group"
  }
}