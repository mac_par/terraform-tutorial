terraform {
  required_version = ">= 0.12, < 0.13"
  backend "s3" {
    bucket = "terraform-state-mkbucket"
    region = "eu-west-1"
    key = "terraform/indepth/p04"
  }
}

provider "aws" {
  region = var.region
}