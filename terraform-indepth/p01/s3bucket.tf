resource "aws_s3_bucket" "testowy-bucket" {
  region = var.region
  bucket = "testowy-bucket-mc1"
  versioning {
    enabled = true
  }

  lifecycle {
    prevent_destroy = false
  }

  tags = {
    Name = "moj-testowy-bucket"
  }
}