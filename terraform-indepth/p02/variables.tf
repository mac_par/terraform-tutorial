variable "ami_image" {
  default = "ami-0ea3405d2d2522162"
  type = string
}

variable "region" {
  default = "eu-west-1"
  type = string
}

variable "instance_type" {
  type = string
}