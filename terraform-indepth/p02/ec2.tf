resource "aws_instance" "testowe" {
  ami = var.ami_image
  instance_type = var.instance_type
  count = 3

  tags = {
  Name = "testowa-instancja-${count.index}"
}
}