terraform {
  required_version = ">= 0.12, < 0.13"
}

provider "aws" {
  region = "eu-central-1"
  version = "~> 2.0"
}

resource "aws_instance" "single-web-server" {
  instance_type = "t2.micro"
  ami = "ami-0d4c3eabb9e72650a"

  vpc_security_group_ids = [aws_security_group.http_group.id,
                            aws_security_group.ssh_group.id]
  user_data = <<-EOF
              #!/bin/bash
              echo "Hello World" > index.html
              nohup busybox http -f -p 8080 &
              EOF

  tags = {
    Name = "web-server"
  }
}

resource "aws_security_group" "http_group" {
  name = var.tcp_security_gropu
  description = "http group"
  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_security_group" "ssh_group" {
  name = var.ssh_security_gropu
  description = "allows ssh traffic"
  ingress {
    from_port = 22
    protocol = "tcp"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "Allow SSH"
  }
}

output "public_ip" {
  value = aws_instance.single-web-server.public_ip
  description = "Public ip"
}

variable "tcp_security_gropu" {
  description = "8080 access"
  type        = string
  default     = "terraform-example-tcp"
}

variable "ssh_security_gropu" {
  description = "22 access"
  type        = string
  default     = "terraform-example-ssh"
}
