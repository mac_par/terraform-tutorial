terraform {
  required_version = ">= 0.12, < 0.13"
}

provider "aws" {
  region = "eu-central-1"
  version = "~> 2.0"
}

resource "aws_instance" "example" {
  ami = "ami-0d4c3eabb9e72650a"
  instance_type = "t2.micro"
  tags = {
    Name = "probne-ec2"
  }
}