variable "server_port" {
  default = 8080
  description = "Server port number"
  type = number
}

variable "alb_port" {
  default = 80
  description = "Alb listener port number"
  type = number
}