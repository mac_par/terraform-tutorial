output "alb_dns_name" {
  value = aws_alb.tr_alb.dns_name
  description = "ALB domain name"
}