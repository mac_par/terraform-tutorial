terraform {
  required_version = ">= 0.12, < 0.13"
}

provider "aws" {
  region = "eu-central-1"
}

#data section
data "aws_vpc" "def_vpc" {
  default = true
}

data "aws_subnet_ids" "def_subnets_ids" {
  vpc_id = data.aws_vpc.def_vpc.id
}

#launch configuration
resource "aws_launch_configuration" "tr_launch_conf" {
  image_id = "ami-0d4c3eabb9e72650a"
  instance_type = "t2.micro"
  security_groups = [aws_security_group.http_access.id]
  name = "instance launch configuration"
  user_data = <<-EOF
              #!/bin/bash
              echo "Hello World!" > index.html
              nohup busybox http -f -p ${var.server_port} &
              EOF
  lifecycle {
    create_before_destroy = true
  }
}

#scalling grouop
resource "aws_autoscaling_group" "tr_scalling_group" {
  min_size = 2
  max_size = 4
  desired_capacity = 2
  launch_configuration = aws_launch_configuration.tr_launch_conf.name
  #
  load_balancers = []
  target_group_arns = [aws_alb_target_group.tr-target-group.arn]
  vpc_zone_identifier = data.aws_subnet_ids.def_subnets_ids.ids
  health_check_type = "ELB"
  tag {
    key = "Name"
    propagate_at_launch = true
    value = "Tr_asg_group"
  }
}

#alb-resource
resource "aws_alb" "tr_alb" {
  name = "tr-alb-example"
  load_balancer_type = "application"
  security_groups = [aws_security_group.alb_security_group.id]
  subnets = data.aws_subnet_ids.def_subnets_ids.ids
}

resource "aws_alb_listener" "tr_alb_listener" {
load_balancer_arn = aws_alb.tr_alb.arn

  default_action {
    type = "fixed-response"
    fixed_response {
      content_type = "text/plain"
      message_body = "404 Not Found ;("
      status_code = "404"
    }
  }

  port = var.alb_port
  protocol = "HTTP"
}

#alb target group
resource "aws_alb_target_group" "tr-target-group" {
  name = "tr-main-target-group"
  port = var.server_port
  protocol = "HTTP"
  vpc_id = data.aws_vpc.def_vpc.id
  health_check {
    enabled = true
    healthy_threshold = 2
    unhealthy_threshold = 2
    path = "/"
    protocol = "HTTP"
    matcher = "200"
    interval = 15
    timeout = 3
  }
}

#alb listener rul
resource "aws_alb_listener_rule" "tr_listener_rule" {
  action {
    type = "forward"
    target_group_arn = aws_alb_target_group.tr-target-group.arn
  }

  condition {
    field = "path-pattern"
    values = ["/*"]
  }
  priority = 100
  listener_arn = aws_alb_listener.tr_alb_listener.arn
}

#http access security group
resource "aws_security_group" "http_access" {
  description = "Http access on server port 8080"
  name = "launch tr group security group"

  ingress {
    from_port = var.server_port
    protocol = "tcp"
    to_port = var.server_port
    security_groups = [aws_security_group.alb_security_group.id]
  }
}

resource "aws_security_group" "alb_security_group" {
  description = "TR alb security group"
  ingress {
    from_port = var.alb_port
    protocol = "tcp"
    to_port = var.alb_port
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}