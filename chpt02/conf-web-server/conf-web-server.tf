terraform {
  required_version = ">= 0.12, < 0.13"
}

provider "aws" {
  region = "eu-central-1"
  version = "~> 2.0"
}

resource "aws_instance" "tmp-server" {
  instance_type = "t2.micro"
  ami = "ami-0d4c3eabb9e72650a"
  tags = {
    Name = "web-serveren"
  }

  user_data = <<-EOF
              #!/bin/bash
              echo "HelloWorld" > index.html
              nohup busybox http -f -p ${var.port_number_tcp} &
              EOF
  vpc_security_group_ids = [aws_security_group.tcp_sq.id]
}

resource "aws_security_group" "tcp_sq" {
  description = "tcp security group"
  name = var.http_group_name
  ingress {
    from_port = var.port_number_tcp
    protocol = "tcp"
    to_port = var.port_number_tcp
    cidr_blocks = ["0.0.0.0/0"]
  }
}

variable "http_group_name" {
  default = "tmp-http-group"
  description = "http security group name"
  type = string
}

variable "port_number_tcp" {
  default = 8080
  description = "allowed tcp port number"
  type = number
}

output "public_id" {
  value = aws_instance.tmp-server.public_ip
  description = "The public IP of the Instance"
}