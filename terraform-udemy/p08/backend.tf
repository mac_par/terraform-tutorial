terraform {
  backend "s3" {
    bucket = "terraform-state-mkbucket"
    key = "terraform/p08/tfstate.tf"
    region = "eu-west-1"
  }
}