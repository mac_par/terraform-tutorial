#main vpc
resource "aws_vpc" "main_vpc" {
  cidr_block = "10.0.0.0/16"
  instance_tenancy = "default"
  enable_classiclink = false
  enable_dns_support = true
  enable_dns_hostnames = true
  tags = {
    Name ="main-vpc"
  }
}

#subnets
##public
resource "aws_subnet" "main_public_1" {
  cidr_block = "10.0.1.0/24"
  vpc_id = aws_vpc.main_vpc.id
  availability_zone = "eu-west-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "main-public-1"
  }
}

resource "aws_subnet" "main_public_2" {
  vpc_id = aws_vpc.main_vpc.id
  cidr_block = "10.0.2.0/24"
  map_public_ip_on_launch = true
  availability_zone = "eu-west-1b"

  tags = {
    Name = "main-public-2"
  }
}

resource "aws_subnet" "main_public_3" {
  cidr_block = "10.0.3.0/24"
  vpc_id = aws_vpc.main_vpc.id
  map_public_ip_on_launch = true
  availability_zone = "eu-west-1c"

  tags = {
    Name = "main-public-3"
  }
}

##private subnets

resource "aws_subnet" "main_private_1" {
  cidr_block = "10.0.4.0/24"
  vpc_id = aws_vpc.main_vpc.id
  map_public_ip_on_launch = false
  availability_zone = "eu-west-1a"

  tags = {
    Name = "main-private-1"
  }
}

resource "aws_subnet" "main-private-2" {
  cidr_block = "10.0.5.0/24"
  vpc_id = aws_vpc.main_vpc.id
  map_public_ip_on_launch = false
  availability_zone = "eu-west-1b"

  tags = {
    Name = "main-private-2"
  }
}

resource "aws_subnet" "main-private-3" {
  cidr_block = "10.0.6.0/24"
  vpc_id = aws_vpc.main_vpc.id
  map_public_ip_on_launch = false
  availability_zone = "eu-west-1c"

  tags = {
    Name = "main-private-3"
  }
}

#internet GateWay
resource "aws_internet_gateway" "main-gw" {
  vpc_id = aws_vpc.main_vpc.id
  tags = {
    Name = "main-gw"
  }
}

#route tables
resource "aws_route_table" "main_route_table" {
  vpc_id = aws_vpc.main_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main-gw.id
  }

  tags = {
    Name = "main-route-table"
  }
}

#public route table associations
resource "aws_route_table_association" "main_public_1_a" {
  route_table_id = aws_route_table.main_route_table.id
  subnet_id = aws_subnet.main_public_1.id
}

resource "aws_route_table_association" "main_public_2_a" {
  route_table_id = aws_route_table.main_route_table.id
  subnet_id = aws_subnet.main_public_2.id
}

resource "aws_route_table_association" "main_public_3_a" {
  route_table_id = aws_route_table.main_route_table.id
  subnet_id = aws_subnet.main_public_3.id
}