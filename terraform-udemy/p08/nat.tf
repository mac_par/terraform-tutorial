resource "aws_eip" "nat" {
  vpc = true
}

resource "aws_nat_gateway" "nat_gw" {
  allocation_id = aws_eip.nat.id
  subnet_id = aws_subnet.main_public_1.id
  depends_on = [aws_internet_gateway.main-gw]
}

resource "aws_route_table" "nat_route_table" {
  vpc_id = aws_vpc.main_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat_gw.id
  }

  tags = {
    Name = "main-private-gw-1"
  }
}

resource "aws_route_table_association" "main-private-1-a" {
  route_table_id = aws_route_table.nat_route_table.id
  subnet_id = aws_subnet.main_private_1.id
}

resource "aws_route_table_association" "main-private-2-a" {
  route_table_id = aws_route_table.nat_route_table.id
  subnet_id = aws_subnet.main-private-2.id
}

resource "aws_route_table_association" "main-private-3-a" {
  route_table_id = aws_route_table.nat_route_table.id
  subnet_id = aws_subnet.main-private-3.id
}