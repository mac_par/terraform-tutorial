resource "aws_security_group" "from-europen" {
  name = "from-europe-sg"
  ingress {
    from_port = 443
    protocol = "TCP"
    to_port = 443
    cidr_blocks = data.aws_ip_ranges.eu-ip-range.cidr_blocks
  }

  tags = {
    create-date = data.aws_ip_ranges.eu-ip-range.create_date
    sync-token  = data.aws_ip_ranges.eu-ip-range.sync_token
  }
}