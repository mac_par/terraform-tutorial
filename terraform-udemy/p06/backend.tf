terraform {
  backend "s3" {
    region = "eu-west-1"
    bucket = "terraform-state-mkbucket"
    key = "terraform/p06"
  }
}