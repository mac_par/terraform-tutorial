data "aws_ip_ranges" "eu-ip-range" {
  services = ["ec2"]
  regions = [var.region]
}