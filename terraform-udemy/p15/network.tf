resource "aws_vpc" "main_vpc" {
  cidr_block = "10.0.0.0/16"
  enable_classiclink = false
  enable_dns_hostnames = true
  enable_dns_support = true
  instance_tenancy = "default"
  tags = {
    Name = "my_main_vpc"
  }
}

#public part
resource "aws_subnet" "main_public_1" {
  cidr_block = "10.0.1.0/24"
  vpc_id = aws_vpc.main_vpc.id
  availability_zone = "eu-west-1a"
  map_public_ip_on_launch = true
  tags = {
    Name = "main-public-1"
  }
}

resource "aws_subnet" "main_public_2" {
  cidr_block = "10.0.2.0/24"
  vpc_id = aws_vpc.main_vpc.id
  availability_zone = "eu-west-1b"
  map_public_ip_on_launch = true
  tags = {
    Name = "main-public-2"
  }
}

resource "aws_subnet" "main_public_3" {
  cidr_block = "10.0.3.0/24"
  vpc_id = aws_vpc.main_vpc.id
  map_public_ip_on_launch = true
  availability_zone = "eu-west-1c"

  tags = {
    Name = "main-public-3"
  }
}

resource "aws_internet_gateway" "gateway" {
  vpc_id = aws_vpc.main_vpc.id
  tags = {
    Name = "public_gateway"
  }
}

resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.main_vpc.id
  route {
    gateway_id = aws_internet_gateway.gateway.id
    cidr_block = "0.0.0.0/0"
  }

  tags = {
    Name = "public-route-table"
  }
}

resource "aws_route_table_association" "main_public_1_assoc" {
  route_table_id = aws_route_table.public_route_table.id
  subnet_id = aws_subnet.main_public_1.id
}

resource "aws_route_table_association" "main_public_2_assoc" {
  route_table_id = aws_route_table.public_route_table.id
  subnet_id = aws_subnet.main_public_2.id
}

resource "aws_route_table_association" "main_public_3_assoc" {
  route_table_id = aws_route_table.public_route_table.id
  subnet_id = aws_subnet.main_public_3.id
}

#private part
resource "aws_subnet" "main_private_1" {
  cidr_block = "10.0.4.0/24"
  vpc_id = aws_vpc.main_vpc.id
  availability_zone = "eu-west-1a"
  map_public_ip_on_launch = false
  tags = {
    Name = "main-private-1"
  }
}

resource "aws_subnet" "main_private_2" {
  cidr_block = "10.0.5.0/24"
  vpc_id = aws_vpc.main_vpc.id
  availability_zone = "eu-west-1b"
  map_public_ip_on_launch = false
  tags = {
    Name = "main-private-2"
  }
}

resource "aws_subnet" "main_private_3" {
  cidr_block = "10.0.6.0/24"
  vpc_id = aws_vpc.main_vpc.id
  availability_zone = "eu-west-1c"
  map_public_ip_on_launch = false
  tags = {
    Name = "main-private-3"
  }
}

resource "aws_eip" "mein_nat" {
  vpc = true
  tags  = {
    Name = "mein-private-nat"
  }
}

resource "aws_nat_gateway" "die_naten" {
  allocation_id = aws_eip.mein_nat.id
  subnet_id = aws_subnet.main_public_1.id
  depends_on = [aws_internet_gateway.gateway]
}

resource "aws_route_table" "main_private_route_table" {
  vpc_id = aws_vpc.main_vpc.id
  route {
    gateway_id = aws_nat_gateway.die_naten.id
    cidr_block = "0.0.0.0/0"
  }

  tags = {
    Name = "private-route-table"
  }
}

resource "aws_route_table_association" "main_private_1_assoc" {
  subnet_id = aws_subnet.main_private_1.id
  route_table_id = aws_route_table.main_private_route_table.id
}

resource "aws_route_table_association" "main_private_2_assoc" {
  subnet_id = aws_subnet.main_private_2.id
  route_table_id = aws_route_table.main_private_route_table.id
}

resource "aws_route_table_association" "main_private_3_assoc" {
  subnet_id = aws_subnet.main_private_3.id
  route_table_id = aws_route_table.main_private_route_table.id
}