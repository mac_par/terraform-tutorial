resource "aws_security_group" "instance_security_group" {
  name = "instance-security-group"
  description = "security group that allows ssh and http communication"
  vpc_id = aws_vpc.main_vpc.id

  ingress {
    from_port = var.ssh_port
    protocol = "tcp"
    to_port = var.ssh_port
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = var.app_port
    protocol = "tcp"
    to_port = var.app_port
    security_groups = [aws_security_group.alb_security_group.id]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "instance-security-group"
  }
}

resource "aws_security_group" "alb_security_group" {
  name = "alb-security-group"
  description = "classic load balancer application security group"
  vpc_id = aws_vpc.main_vpc.id
  ingress {
    from_port = var.app_port
    protocol = "tcp"
    to_port = var.app_port
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "elb-security-group"
  }
}
