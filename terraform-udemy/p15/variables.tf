variable "region" {
  type = string
  default = "eu-west-1"
}

variable "instance_type" {
  type = string
  default = "t2.micro"
}

variable "ami_image_id" {
  type = string
  default = "ami-035966e8adab4aaad"
}

variable "key_name" {
  type = string
  default = "mein_key"
}

variable "key_path" {
  type = string
  default = "keys/mykey.pub"
}

variable "ssh_port" {
  type = number
  default = 22
}

variable "app_port" {
  type = number
  default = 80
}