resource "aws_elb" "mein_alb" {
  name = "mein-alb"
  security_groups = [aws_security_group.alb_security_group.id]
  subnets = [aws_subnet.main_public_1.id, aws_subnet.main_public_2.id]
  cross_zone_load_balancing = true

  listener {
    instance_port = var.app_port
    instance_protocol = "http"
    lb_port = var.app_port
    lb_protocol = "http"
  }

  connection_draining = true
  connection_draining_timeout = 400

  health_check {
    healthy_threshold = 2
    interval = 30
    target = "HTTP:80/"
    timeout = 3
    unhealthy_threshold = 2
  }
  tags = {
    Name = "mein_eine-kleine-elb"
  }
}