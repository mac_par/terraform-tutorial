resource "aws_launch_configuration" "mein_instance_conf" {
  name = "mein-eine-kleine-instanten"
  image_id = var.ami_image_id
  instance_type = var.instance_type
  security_groups = [aws_security_group.instance_security_group.id]
  key_name = aws_key_pair.mein_key_pair.key_name
  user_data = <<-EOF
  #!/bin/bash
  sudo -s
  apt-get upgrade
  apt-get install -y mysql-client python-pip python-dev
  apt-get update
  pip install awscli
  apt-get -y install nginx
  MYIP=`ifconfig | grep 'addr:10' | awk '{ print $2 }' | cut -d ':' -f2`
  echo 'this is: '$MYIP > /var/www/html/index.html"
EOF

  lifecycle {
    create_before_destroy = true
  }
}


resource "aws_autoscaling_group" "mein_scalen_groupen" {
  name_prefix = "Jarek-"
  min_size = 1
  max_size = 2
  desired_capacity = 2
  vpc_zone_identifier = [aws_subnet.main_public_1.id, aws_subnet.main_public_2.id]
  launch_configuration = aws_launch_configuration.mein_instance_conf.name
  load_balancers = [aws_elb.mein_alb.name]
  health_check_grace_period = 300
  health_check_type = "ELB"
  force_delete = true

  tag {
    key                 = "Name"
    value               = "ec2 instance"
    propagate_at_launch = true
  }
}