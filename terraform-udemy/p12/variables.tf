variable "region" {
  type = string
  default = "eu-west-1"
}

variable "image_id" {
  type = string
  default = "ami-035966e8adab4aaad"
}

variable "instance_type" {
  type = string
  default = "t2.micro"
}

variable "key_path" {
  type = string
  default = "keys/mykey.pub"
}

variable "key_name" {
  type = string
  default = "mein-key"
}

variable "ssh_port" {
  type = number
  default = 22
}

variable "rds_port" {
  type = number
  default = 3306
}

variable "rds_instance_class" {
  type = string
  default = "db.t2.micro"
}

variable "rds_db_version" {
  type = string
  default = "10.3.8"
}

variable "RDS_USER" {
  type = string
  default = "root"
}

variable "RDS_PASSWD" {}