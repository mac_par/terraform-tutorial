resource "aws_security_group" "instance-sg" {
  vpc_id = aws_vpc.main_vpc.id
  name = "allow-ssh"
  description = "allows communication via ssh port"
  ingress {
    from_port = var.ssh_port
    protocol = "tcp"
    to_port = var.ssh_port
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow-ssh-sg"
  }
}

resource "aws_security_group" "rds_security_group" {
  vpc_id = aws_vpc.main_vpc.id
  name = "rds-sg"
  description = "allows communication on port ${var.rds_port}"

  ingress {
    from_port = var.rds_port
    protocol = "tcp"
    to_port = var.rds_port
    security_groups = [aws_security_group.instance-sg.id]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
    self = true
  }

  tags = {
    Name = "rds-mariadb-sg"
  }
}