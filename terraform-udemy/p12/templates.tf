data "template_file" "init-script" {
  template = file("scripts/init.sh")
}

data "template_cloudinit_config" "user_data" {
  base64_encode = false
  gzip = false
  part {
    content_type = "text/x-shellscript"
    content = data.template_file.init-script.rendered
  }
}