resource "aws_db_subnet_group" "maria_db_subnet_group" {
  name = "maria-db-subnet"
  description = "mariaDB subnet group"
  subnet_ids = [aws_subnet.main_private_1.id, aws_subnet.main_private_2.id]
}

resource "aws_db_parameter_group" "maria_db_parameter_group" {
  name = "maria-db-param-group"
  family = "mariadb10.3"
  description = "MariaDb parameter group"

  parameter {
    name  = "max_allowed_packet"
    value = "16777216"
  }
}

resource "aws_db_instance" "mariadb_instance" {
  allocated_storage = 10
  engine = "mariadb"
  engine_version = var.rds_db_version
  instance_class = var.rds_instance_class
  identifier = "mariadb"
  name = "mariadb"
  username = var.RDS_USER
  password = var.RDS_PASSWD
  multi_az = false
  parameter_group_name = aws_db_parameter_group.maria_db_parameter_group.name
  db_subnet_group_name = aws_db_subnet_group.maria_db_subnet_group.name
  vpc_security_group_ids = [aws_security_group.rds_security_group.id]
  storage_type = "gp2"
  backup_retention_period = 30
  availability_zone = aws_subnet.main_private_1.availability_zone
  skip_final_snapshot = true
  tags = {
    Name = "mein-maria"
  }
}