output "instance_ip" {
  value = aws_instance.mein_instance.public_ip
}

output "rds_endpoint" {
  value = aws_db_instance.mariadb_instance.endpoint
}