resource "aws_instance" "mein_instance" {
  ami = var.image_id
  instance_type = var.instance_type
  subnet_id = aws_subnet.main_public_1.id
  vpc_security_group_ids = [aws_security_group.instance-sg.id]
  key_name = aws_key_pair.key_pir.key_name
  user_data = data.template_cloudinit_config.user_data.rendered
}