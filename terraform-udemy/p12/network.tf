resource "aws_vpc" "main_vpc" {
  cidr_block = "10.0.0.0/16"
  enable_classiclink = false
  enable_dns_hostnames = true
  enable_dns_support = true
  instance_tenancy = "default"
  tags  = {
    Name = "main-vpc"
  }
}

resource "aws_subnet" "main_public_1" {
  cidr_block = "10.0.1.0/24"
  vpc_id = aws_vpc.main_vpc.id
  availability_zone = "eu-west-1a"
  map_public_ip_on_launch = true
  tags = {
    Name = "main_public_1"
  }
}

resource "aws_subnet" "main_public_2" {
  cidr_block = "10.0.2.0/24"
  vpc_id = aws_vpc.main_vpc.id
  availability_zone = "eu-west-1b"
  map_public_ip_on_launch = true
  tags = {
    Name = "main_public_2"
  }
}

resource "aws_subnet" "main_public_3" {
  cidr_block = "10.0.3.0/24"
  vpc_id = aws_vpc.main_vpc.id
  availability_zone = "eu-west-1c"
  map_public_ip_on_launch = true
  tags = {
    Name = "main_public_3"
  }
}

#public gateway
resource "aws_internet_gateway" "main_gateway" {
  vpc_id = aws_vpc.main_vpc.id
  tags = {
    Name = "main_gateway"
  }
}

#route table
resource "aws_route_table" "main_public_route_table" {
  vpc_id = aws_vpc.main_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main_gateway.id
  }

  tags = {
    Name = "jakas_tam_route_table"
  }
}

resource "aws_route_table_association" "main_public_1_assoc" {
  route_table_id = aws_route_table.main_public_route_table.id
  subnet_id = aws_subnet.main_public_1.id
}

resource "aws_route_table_association" "main_public_2_assoc" {
  route_table_id = aws_route_table.main_public_route_table.id
  subnet_id = aws_subnet.main_public_2.id
}

resource "aws_route_table_association" "main_public_3_assoc" {
  route_table_id = aws_route_table.main_public_route_table.id
  subnet_id = aws_subnet.main_public_3.id
}

resource "aws_subnet" "main_private_1" {
  cidr_block = "10.0.4.0/24"
  vpc_id = aws_vpc.main_vpc.id
  availability_zone = "eu-west-1a"
  map_public_ip_on_launch = false
  tags = {
    Name = "main_private_1"
  }
}

resource "aws_subnet" "main_private_2" {
  cidr_block = "10.0.5.0/24"
  vpc_id = aws_vpc.main_vpc.id
  map_public_ip_on_launch = false
  availability_zone = "eu-west-1b"
  tags = {
    Name = "main_private_2"
  }
}

resource "aws_subnet" "main_private_3" {
  cidr_block = "10.0.6.0/24"
  vpc_id = aws_vpc.main_vpc.id
  availability_zone = "eu-west-1c"
  map_public_ip_on_launch = false
  tags = {
    Name = "main_private_3"
  }
}

#nat part
resource "aws_eip" "nat" {
  vpc = true
}

resource "aws_nat_gateway" "main_nat_gate" {
  allocation_id = aws_eip.nat.id
  subnet_id = aws_subnet.main_public_1.id
  depends_on = [aws_internet_gateway.main_gateway]
}

resource "aws_route_table" "main_private_route_table" {
  vpc_id = aws_vpc.main_vpc.id
  route {
    gateway_id = aws_nat_gateway.main_nat_gate.id
    cidr_block = "0.0.0.0/24"
  }

  tags = {
    Name = "main_private_route_table"
  }
}

resource "aws_route_table_association" "main_private_1_assoc" {
  route_table_id = aws_route_table.main_private_route_table.id
  subnet_id = aws_subnet.main_private_1.id
}


resource "aws_route_table_association" "main_private_2_assoc" {
  route_table_id = aws_route_table.main_private_route_table.id
  subnet_id = aws_subnet.main_private_2.id
}

resource "aws_route_table_association" "main_private_3_assoc" {
  route_table_id = aws_route_table.main_private_route_table.id
  subnet_id = aws_subnet.main_private_3.id
}