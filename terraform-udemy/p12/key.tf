resource "aws_key_pair" "key_pir" {
  public_key = file(var.key_path)
  key_name = var.key_name
}