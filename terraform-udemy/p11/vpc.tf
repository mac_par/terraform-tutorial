resource "aws_vpc" "main_vpc" {
  cidr_block = "10.0.0.0/16"
  instance_tenancy = "default"
  enable_dns_support = true
  enable_classiclink = false
  enable_dns_hostnames = true
  tags = {
    Name = "main-vpc-1"
  }
}

resource "aws_subnet" "public_main_1" {
  vpc_id = aws_vpc.main_vpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "eu-west-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "public-main-1"
  }
}

resource "aws_subnet" "public_main_2" {
  vpc_id = aws_vpc.main_vpc.id
  cidr_block = "10.0.2.0/24"
  availability_zone = "eu-west-1b"
  map_public_ip_on_launch = true

  tags = {
    Name = "public-main-2"
  }
}

resource "aws_subnet" "public_main_3" {
  cidr_block = "10.0.3.0/24"
  vpc_id = aws_vpc.main_vpc.id
  availability_zone = "eu-west-1c"
  map_public_ip_on_launch = true

  tags = {
    Name = "public-main-3"
  }
}

resource "aws_subnet" "private_main_1" {
  cidr_block = "10.0.4.0/24"
  vpc_id = aws_vpc.main_vpc.id
  availability_zone = "eu-west-1a"
  map_public_ip_on_launch = false

  tags = {
    Name = "private-main-1"
  }
}

resource "aws_subnet" "private_main_2" {
  vpc_id = aws_vpc.main_vpc.id
  cidr_block = "10.0.5.0/24"
  availability_zone = "eu-west-1b"
  map_public_ip_on_launch = false

  tags = {
    Name = "private-main-2"
  }
}

resource "aws_subnet" "private_main_3" {
  cidr_block = "10.0.6.0/24"
  vpc_id = aws_vpc.main_vpc.id
  map_public_ip_on_launch = false
  availability_zone = "eu-west-1c"

  tags = {
    Name = "private-main-3"
  }
}

resource "aws_internet_gateway" "main_gateway" {
  vpc_id = aws_vpc.main_vpc.id

  tags = {
    Name = "main_interner-gateway"
  }
}

resource "aws_route_table" "main_route_table" {
  vpc_id = aws_vpc.main_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main_gateway.id
  }

  tags = {
    Name = "main-route-table"
  }
}

#associations
resource "aws_route_table_association" "main_public_1_assoc" {
  route_table_id = aws_route_table.main_route_table.id
  subnet_id = aws_subnet.public_main_1.id
}


resource "aws_route_table_association" "main_public_2_assoc" {
  route_table_id = aws_route_table.main_route_table.id
  subnet_id = aws_subnet.public_main_2.id
}

resource "aws_route_table_association" "main_public_3_assoc" {
  subnet_id = aws_subnet.public_main_3.id
  route_table_id = aws_route_table.main_route_table.id
}

resource "aws_eip" "nat" {
  vpc = true
}

resource "aws_nat_gateway" "private_gateway" {
  allocation_id = aws_eip.nat.id
  subnet_id = aws_subnet.public_main_1.id
  depends_on = [aws_internet_gateway.main_gateway]
}

resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.main_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.private_gateway.id
  }

  tags = {
    Name = "private-route-table"
  }
}

resource "aws_route_table_association" "private_main_1_assoc" {
  route_table_id = aws_route_table.private_route_table.id
  subnet_id = aws_subnet.private_main_1.id
}

resource "aws_route_table_association" "private_main_2_assoc" {
  route_table_id = aws_route_table.private_route_table.id
  subnet_id = aws_subnet.private_main_2.id
}

resource "aws_route_table_association" "private_main_3_assoc" {
  route_table_id = aws_route_table.private_route_table.id
  subnet_id = aws_subnet.private_main_3.id
}