terraform {
  required_version = ">= 0.12"
  backend "s3" {
    bucket = "terraform-state-mkbucket"
    region = "eu-west-1"
    key = "terraform/p11/tfstate.tf"
  }
}

provider "aws" {
  region = var.region
}