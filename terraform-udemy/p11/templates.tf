data "template_file" "init_script" {
  template = file("scripts/init.cfg")
  vars = {
    REGION = var.region
  }
}

data "template_file" "shell-script" {
  template = file("scripts/volumes.sh")
  vars  = {
    DEVICE = var.instance_device_name
  }
}

data "template_cloudinit_config" "cloud-init-example" {
  gzip = false
  base64_encode = false

  part {
    filename = "init.cfg"
    content = data.template_file.init_script.rendered
    content_type = "text/cloud-config"
  }

  part {
    content = data.template_file.shell-script.rendered
    content_type = "text/x-shellscript"
  }
}