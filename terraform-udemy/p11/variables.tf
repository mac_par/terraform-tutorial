variable "region" {
  type = string
  default = "eu-west-1"
}

variable "ami_image" {
  type = string
  default = "ami-035966e8adab4aaad"
}

variable "instance_type" {
  type = string
  default = "t2.micro"
}

variable "ssh_port" {
  type = number
  default = 22
}

variable "volume_size" {
  type = number
  default = 10
}

variable "key_path" {
  type = string
  default = "mykey.pub"
}

variable "key_name" {
  type = string
  default = "mein_key"
}

variable "instance_device_name" {
  default = "/dev/xvdh"
}