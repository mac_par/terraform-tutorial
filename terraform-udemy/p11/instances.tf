resource "aws_key_pair" "my_key" {
  public_key = file(var.key_path)
  key_name = var.key_name
}

resource "aws_security_group" "allow_ssh" {
  vpc_id = aws_vpc.main_vpc.id
  name = "allow-ssh-sg"
  description = "Allows usage of ssh port"

  ingress {
    from_port = var.ssh_port
    protocol = "tcp"
    to_port = var.ssh_port
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = [
      "0.0.0.0/0"]
  }
}

resource "aws_instance" "mein_instance" {
  ami = var.ami_image
  instance_type = var.instance_type
  vpc_security_group_ids = [
    aws_security_group.allow_ssh.id]
  subnet_id = aws_subnet.public_main_1.id
  key_name = aws_key_pair.my_key.key_name

  user_data = data.template_cloudinit_config.cloud-init-example.rendered
}

resource "aws_ebs_volume" "additional_volumne" {
  availability_zone = "eu-west-1a"
  type = "gp2"
  size = var.volume_size

  tags = {
    Name = "additional-volume"
  }
}

resource "aws_volume_attachment" "attach_additiona" {
  device_name = var.instance_device_name
  instance_id = aws_instance.mein_instance.id
  volume_id = aws_ebs_volume.additional_volumne.id
  skip_destroy = true
}