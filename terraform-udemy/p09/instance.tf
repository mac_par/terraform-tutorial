resource "aws_key_pair" "mein_key" {
  public_key = file(var.key_path)
  key_name = "mein_key_pair"
}

resource "aws_security_group" "mein_ssh_sg" {
  name = "instance-ssh-security-group"
  description = "taka tam proba z sg"
  vpc_id = aws_vpc.main_vpc.id

#input
  ingress {
    from_port = var.ssh_port
    protocol = "tcp"
    to_port = var.ssh_port
    cidr_blocks = ["0.0.0.0/0"]
  }

#output
  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "mein_instance" {
  ami = var.ami_id
  instance_type = var.instance_type
  vpc_security_group_ids = [aws_security_group.mein_ssh_sg.id]
  subnet_id = aws_subnet.main_public_1.id
  key_name = aws_key_pair.mein_key.key_name
}