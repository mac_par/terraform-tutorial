variable "region" {
  type = string
  default = "eu-west-1"
}

variable "key_path" {
  type = string
  default = "keys/mykey.pub"
}

variable "instance_type" {
  type = string
  default = "t2.micro"
}

variable "ami_id" {
  type = string
  default = "ami-04d5cc9b88f9d1d39"
}

variable "ssh_port" {
  type = number
  default = 22
}