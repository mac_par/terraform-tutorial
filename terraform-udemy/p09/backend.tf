terraform {
  backend "s3" {
    bucket = "terraform-state-mkbucket"
    key = "terraform/p09/terra-state.tf"
    region = "eu-west-1"
  }
}