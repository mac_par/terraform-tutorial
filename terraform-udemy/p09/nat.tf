resource "aws_eip" "nat" {
  vpc = true
}

resource "aws_nat_gateway" "nat-gw" {
  allocation_id = aws_eip.nat.id
  subnet_id = aws_subnet.main_public_1.id
  depends_on = [aws_internet_gateway.main_gateway]
}

resource "aws_route_table" "main_private_route_table" {
  vpc_id = aws_vpc.main_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat-gw.id
  }

  tags = {
    Name = "main-private-route-table"
  }
}

resource "aws_route_table_association" "main_private_1_a" {
  route_table_id = aws_route_table.main_private_route_table.id
  subnet_id = aws_subnet.main_private_1.id
}

resource "aws_route_table_association" "main_private_2_a" {
  route_table_id = aws_route_table.main_private_route_table.id
  subnet_id = aws_subnet.main_private_2.id
}

resource "aws_route_table_association" "main_private_3_a" {
  route_table_id = aws_route_table.main_private_route_table.id
  subnet_id = aws_subnet.main_private_3.id
}