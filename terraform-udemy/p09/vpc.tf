resource "aws_vpc" "main_vpc" {
  cidr_block = "10.0.0.0/16"
  enable_classiclink = false
  enable_dns_hostnames = true
  enable_dns_support = true
  instance_tenancy = "default"

  tags = {
    Name = "main-proba-vpc"
  }
}

#subnets
##public
resource "aws_subnet" "main_public_1" {
  vpc_id = aws_vpc.main_vpc.id
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone = "eu-west-1a"

  tags = {
    Name = "main-public-1"
  }
}

resource "aws_subnet" "main_public_2" {
  vpc_id = aws_vpc.main_vpc.id
  cidr_block = "10.0.2.0/24"
  map_public_ip_on_launch = true
  availability_zone = "eu-west-1b"

  tags = {
    Name = "main-public-2"
  }
}

resource "aws_subnet" "main_public_3" {
  cidr_block = "10.0.3.0/24"
  vpc_id = aws_vpc.main_vpc.id
  map_public_ip_on_launch = true
  availability_zone = "eu-west-1c"

  tags = {
    Name = "main-public-3"
  }
}

##private
resource "aws_subnet" "main_private_1" {
  vpc_id = aws_vpc.main_vpc.id
  cidr_block = "10.0.4.0/24"
  map_public_ip_on_launch = false
  availability_zone = "eu-west-1a"

  tags = {
    Name = "main-private-1"
  }
}

resource "aws_subnet" "main_private_2" {
  vpc_id = aws_vpc.main_vpc.id
  cidr_block = "10.0.5.0/24"
  map_public_ip_on_launch = false
  availability_zone = "eu-west-1b"

  tags = {
    Name = "main-private-2"
  }
}

resource "aws_subnet" "main_private_3" {
  cidr_block = "10.0.6.0/24"
  vpc_id = aws_vpc.main_vpc.id
  map_public_ip_on_launch = false
  availability_zone = "eu-west-1c"

  tags = {
    Name = "main-private-3"
  }
}

#internet gateway
resource "aws_internet_gateway" "main_gateway" {
  vpc_id = aws_vpc.main_vpc.id
  tags = {
    Name = "main-gateway"
  }
}

#route table
resource "aws_route_table" "main_route_table" {
  vpc_id = aws_vpc.main_vpc.id
  route {
    gateway_id = aws_internet_gateway.main_gateway.id
    cidr_block = "0.0.0.0/0"
  }

  tags = {
    Name ="main-route-table"
  }
}

#public route table associations
resource "aws_route_table_association" "main-public-1-a" {
  route_table_id = aws_route_table.main_route_table.id
  subnet_id = aws_subnet.main_public_1.id
}

resource "aws_route_table_association" "main-public-2-a" {
  route_table_id = aws_route_table.main_route_table.id
  subnet_id = aws_subnet.main_public_2.id
}

resource "aws_route_table_association" "main-public-3-a" {
  route_table_id = aws_route_table.main_route_table.id
  subnet_id = aws_subnet.main_public_3.id
}