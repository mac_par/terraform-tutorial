terraform {
  backend "s3" {
    bucket = "terraform-state-mkbucket"
    key = "terraform/p05"
    region = "eu-west-1"
  }
}