resource "aws_instance" "my-instance" {
  ami = var.instances_ami[var.region]
  instance_type = "t2.micro"

  tags = {
    Name = var.instance-name
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.my-instance.public_ip} >> private_ips.txt"
  }
}