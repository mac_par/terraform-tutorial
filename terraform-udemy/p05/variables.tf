variable "region" {
  type = string
  default = "eu-west-1"
}

variable "instances_ami" {
  type = map(string)
  default = {
    "eu-west-1" = "ami-04d5cc9b88f9d1d39"
  }
}

variable "instance-name" {
  type = string
  default = "jakas_instancja"
}