variable "region" {
  type = string
  default = "eu-west-1"
}

variable "instance_image" {
  type = map(string)
  default = {
    eu-west-1 = "ami-04d5cc9b88f9d1d39"
  }
  description = "proba-1-instance"
}