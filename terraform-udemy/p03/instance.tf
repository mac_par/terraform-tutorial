resource "aws_instance" "my_instance" {
  ami = lookup(var.instance_image, var.region)
  instance_type = "t2.micro"
  tags = {
    dupa = "cos-tam"
    Name = "jakas-instancja-1"
  }
}