resource "aws_instance" "my_instance" {
  ami = var.tr_ami
  instance_type = var.tr_instance_type

  tags = {
    Name = var.tr_nazwa
    dupa = var.tr_jakis_tekst
  }
}