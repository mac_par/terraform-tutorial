variable "region" {
  type = string
  default = "eu-west-1"
}

variable "ami_type" {
  type = string
  default = "ami-04d5cc9b88f9d1d39"
}

variable "inst_type" {
  type = string
  default = "t2.micro"
}