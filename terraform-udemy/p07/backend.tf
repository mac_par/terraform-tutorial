terraform {
  backend "s3" {
    bucket = "terraform-state-mkbucket"
    key = "terraform/p07/"
    region = "eu-west-1"
  }
}