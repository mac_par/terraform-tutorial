variable "region" {
  type = string
  default = "eu-west-1"
}

variable "ami_image" {
  default = "ami-09266271a2521d06f"
  type = string
}

variable "instance_type" {
  type = string
  default = "t2.micro"
}

variable "key_path" {
  type = string
  default = "keys/mykey.pub"
}

variable "docker-file-path" {
  type = string
  default = "demo/Dockerfile"
}

variable "app_port" {
  default = 80
  type = number
}

variable "ssh_port" {
  default = 22
  type = number
}

variable "ecs_port" {
  type = number
  default = 3000
}

variable "task-definion-template_path" {
  type = string
  default = "templates/app.json.tpl"
}