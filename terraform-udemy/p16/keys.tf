resource "aws_key_pair" "my_key_pair" {
  public_key = file(var.key_path)
  key_name = "my-key-pair"
  lifecycle {
    ignore_changes = [public_key]
  }
}