resource "aws_iam_role" "ec2-assume-role" {
  name = "ec2-assume-role"
  assume_role_policy = <<-EOF
{
  "Version":"2012-10-17",
  "Statement": [
    {
      "Action" : "sts:AssumeRole",
      "Principal" : {
        "Service" : "ec2.amazonaws.com"
      },
      "Effect" : "Allow",
      "Sid" : ""
    }
  ]
}
EOF
}

resource "aws_iam_role" "ecs-assume-role" {
  name = "ecs-assume-role"
  assume_role_policy = <<-EOF
{
  "Version":"2012-10-17",
  "Statement" : [
    {
      "Action" : "sts:AssumeRole",
      "Principal" : {
        "Service" : "ecs.amazonaws.com"
      },
      "Effect" : "Allow",
      "Sid" : ""
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "ecs-ec2-role" {
  name = "ecs-ec2-instance-policy"
  role = aws_iam_role.ec2-assume-role.name
}

resource "aws_iam_role_policy" "ecs-ec2_role-policy" {
  name = "ecs-ec2-role-policy"
  role = aws_iam_role.ec2-assume-role.id
  policy = <<-EOF
{
  "Version": "2012-10-17",
  "Statement" : [
    {
      "Effect" : "Allow",
      "Action" : [
        "ecs:CreateCluster",
          "ecs:DeregisterContainerInstance",
          "ecs:DiscoverPollEndpoint",
          "ecs:Poll",
          "ecs:RegisterContainerInstance",
          "ecs:StartTelemetrySession",
          "ecs:Submit*",
          "ecs:StartTask",
          "ecr:GetAuthorizationToken",
          "ecr:BatchCheckLayerAvailability",
          "ecr:GetDownloadUrlForLayer",
          "ecr:BatchGetImage",
          "logs:CreateLogStream",
          "logs:PutLogEvents"
      ],
      "Resource": "*"
    },
    {
      "Effect" : "Allow",
      "Action" : [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogStreams"
      ],
      "Resource" : [
        "arn:aws:log:*:*:*"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_policy_attachment" "ecs-service-attach" {
  name = "ecs-service-attach-policy"
  policy_arn = data.aws_iam_policy.ecs-container-service-role.arn
  roles = [aws_iam_role.ecs-assume-role.name]
}