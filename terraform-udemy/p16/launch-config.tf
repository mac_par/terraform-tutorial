resource "aws_launch_configuration" "myapp_launch_config" {
  name_prefix = "myapp-"
  image_id = var.ami_image
  instance_type = var.instance_type
  iam_instance_profile = aws_iam_instance_profile.ecs-ec2-role.id
  key_name = aws_key_pair.my_key_pair.key_name
  user_data            = "#!/bin/bash\necho 'ECS_CLUSTER=example-cluster' > /etc/ecs/ecs.config\nstart ecs"
  security_groups = [aws_security_group.ecs_cluster_security_group.id]
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "ecs-myapp-scaling-group" {
  min_size = 1
  max_size = 1
  desired_capacity = 1
  name = "myapp-scaling-group"
  vpc_zone_identifier = [aws_subnet.main_public_1.id, aws_subnet.main_public_2.id]
  launch_configuration = aws_launch_configuration.myapp_launch_config.name
  tag {
    key                 = "Name"
    value               = "ecs-ec2-container"
    propagate_at_launch = true
  }
}