resource "aws_ecs_task_definition" "myapp-task-definition" {
  family = "myapp"
  container_definitions = data.template_file.myapp-task-definition-template.rendered
}

resource "aws_elb" "myapp-alb" {
  name = "myapp-ecs-alb"
  listener {
    instance_port = var.ecs_port
    instance_protocol = "tcp"
    lb_port = var.app_port
    lb_protocol = "tcp"
  }

  health_check {
    healthy_threshold = 3
    interval = 60
    target = "HTTP:3000/"
    timeout = 30
    unhealthy_threshold = 4
  }

  subnets = [
    aws_subnet.main_public_1.id,
    aws_subnet.main_private_2.id]
  security_groups = [
    aws_security_group.alb_securit_group.id]

  connection_draining = true
  connection_draining_timeout = 400
  cross_zone_load_balancing = true
  idle_timeout = 400
  tags = {
    Name = "myapp-elb"
  }
}

resource "aws_ecs_cluster" "myapp-cluster" {
  name = "myapp"
}

resource "aws_ecs_service" "myapp-service-definition" {
  name = "my-app-service"
  task_definition = aws_ecs_task_definition.myapp-task-definition.arn
  cluster = aws_ecs_cluster.myapp-cluster.id
  desired_count = 1
  iam_role = aws_iam_role.ecs-assume-role.arn
  depends_on = [aws_iam_policy_attachment.ecs-service-attach]

  load_balancer {
    container_name = aws_ecs_cluster.myapp-cluster.name
    container_port = var.ecs_port
    elb_name = aws_elb.myapp-alb.name
  }

  lifecycle {
    ignore_changes = [task_definition]
  }
}