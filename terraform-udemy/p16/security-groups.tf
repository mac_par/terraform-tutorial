resource "aws_security_group" "alb_securit_group" {
  name = "alb-security-group"
  description = "alb security group that direct traffic to ecs"
  vpc_id = aws_vpc.my_vpc.id
  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = var.app_port
    protocol = "tcp"
    to_port = var.app_port
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "alb-security-group"
  }
}

resource "aws_security_group" "ecs_cluster_security_group" {
  name = "ecs-security-group"
  description = "ecs cluster app security group"
  vpc_id = aws_vpc.my_vpc.id

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = var.ssh_port
    protocol = "tcp"
    to_port = var.ssh_port
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = var.ecs_port
    protocol = "tcp"
    to_port = var.ecs_port
    security_groups = [aws_security_group.alb_securit_group.id]
  }

  tags = {
    Name = "ecs-security-group"
  }
}