data "template_file" "myapp-task-definition-template" {
  template = file(var.task-definion-template_path)
  vars = {
    REPOSITORY_URL = replace(aws_ecr_repository.myapp.repository_url,"https://" ,"")
  }
}