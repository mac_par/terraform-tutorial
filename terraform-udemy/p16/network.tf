resource "aws_vpc" "my_vpc" {
  cidr_block = "10.0.0.0/16"
  enable_classiclink = false
  enable_dns_hostnames = true
  enable_dns_support = true
  instance_tenancy = "default"
  tags = {
    Name = "my-vpc"
  }
}

resource "aws_subnet" "main_public_1" {
  vpc_id = aws_vpc.my_vpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "${var.region}a"
  map_public_ip_on_launch = true
  tags = {
    Name = "main-public-1"
  }
}

resource "aws_subnet" "main_public_2" {
  vpc_id = aws_vpc.my_vpc.id
  cidr_block = "10.0.2.0/24"
  availability_zone = "${var.region}b"
  map_public_ip_on_launch = true
  tags = {
    Name = "main-public-2"
  }
}
resource "aws_subnet" "main_public_3" {
  vpc_id = aws_vpc.my_vpc.id
  cidr_block = "10.0.3.0/24"
  availability_zone = "${var.region}c"
  map_public_ip_on_launch = true
  tags = {
    Name = "main-public-3"
  }
}
resource "aws_subnet" "main_private_1" {
  vpc_id = aws_vpc.my_vpc.id
  cidr_block = "10.0.4.0/24"
  availability_zone = "${var.region}a"
  map_public_ip_on_launch = false
  tags = {
    Name = "main-private-1"
  }
}
resource "aws_subnet" "main_private_2" {
  vpc_id = aws_vpc.my_vpc.id
  cidr_block = "10.0.5.0/24"
  availability_zone = "${var.region}b"
  map_public_ip_on_launch = false
  tags = {
    Name = "main-private-2"
  }
}
resource "aws_subnet" "main_private_3" {
  vpc_id = aws_vpc.my_vpc.id
  cidr_block = "10.0.6.0/24"
  availability_zone = "${var.region}c"
  map_public_ip_on_launch = false
  tags = {
    Name = "main-private-3"
  }
}

resource "aws_internet_gateway" "my_gateway" {
  vpc_id = aws_vpc.my_vpc.id
  tags = {
    Name = "my_internet-gateway"
  }
}

resource "aws_route_table" "main_public_route_table" {
  vpc_id = aws_vpc.my_vpc.id
  route {
    gateway_id = aws_internet_gateway.my_gateway.id
    cidr_block = "0.0.0.0/0"
  }
}

resource "aws_route_table_association" "main_public_1_assoc" {
  route_table_id = aws_route_table.main_public_route_table.id
  subnet_id = aws_subnet.main_public_1.id
}

resource "aws_route_table_association" "main_public_2_assoc" {
  route_table_id = aws_route_table.main_public_route_table.id
  subnet_id = aws_subnet.main_public_2.id
}

resource "aws_route_table_association" "main_public_3_assoc" {
  route_table_id = aws_route_table.main_public_route_table.id
  subnet_id = aws_subnet.main_public_3.id
}

resource "aws_eip" "main_nat" {
  vpc = true
  tags = {
    Name = "main_nat"
  }
}

resource "aws_nat_gateway" "private_gateway" {
  allocation_id = aws_eip.main_nat.id
  subnet_id = aws_subnet.main_public_1.id
  depends_on = [aws_internet_gateway.my_gateway]
}

resource "aws_route_table" "main_private_route_table" {
  vpc_id = aws_vpc.my_vpc.id
  route {
    gateway_id = aws_nat_gateway.private_gateway.id
    cidr_block = "0.0.0.0/0"
  }
}

resource "aws_route_table_association" "main_private_1_assoc" {
  route_table_id = aws_route_table.main_private_route_table.id
  subnet_id = aws_subnet.main_private_1.id
}

resource "aws_route_table_association" "main_private_2_assoc" {
  route_table_id = aws_route_table.main_private_route_table.id
  subnet_id = aws_subnet.main_private_2.id
}
resource "aws_route_table_association" "main_private_3_assoc" {
  route_table_id = aws_route_table.main_private_route_table.id
  subnet_id = aws_subnet.main_private_3.id
}