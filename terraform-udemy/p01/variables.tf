variable "myvar" {
  type = string
  default = "Hi there mate!"
  description = "Mało istotna zmienna"
}

variable "myvar2" {
  type = map(string)
  default = {
          klucz= "klucz"
  }
}

variable "mylist" {
  type = list(number)
  default = [1,2,3,5]
}

variable "myregion" {
  type = string
  default = "eu-west-1"
}