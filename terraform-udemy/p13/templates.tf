data "template_file" "shell_script" {
  template = file(var.init_script_path)
}

data "template_cloudinit_config" "user_data" {
  base64_encode = false
  gzip = false

  part {
    content_type = "text/x-shellscript"
    content = data.template_file.shell_script.rendered
  }
}