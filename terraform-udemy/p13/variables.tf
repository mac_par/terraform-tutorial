variable "region" {
  type = string
  default = "eu-west-1"
}

variable "key_path" {
  type = string
  default = "keys/mykey.pub"
}

variable "key_name" {
  type = string
  default = "main_key"
}

variable "ssh_port" {
  type = number
  default = 22
}

variable "image_id" {
  type = string
  default = "ami-035966e8adab4aaad"
}

variable "instance_type" {
  type = string
  default = "t2.micro"
}

variable "init_script_path" {
  type = string
  default = "scripts/init.sh"
}