resource "aws_s3_bucket" "moj_fajny_bucket" {
  bucket = "my-fajny-bucket"
  acl = "private"
  tags = {
    Name = "my-fajny-bucket"
  }
}