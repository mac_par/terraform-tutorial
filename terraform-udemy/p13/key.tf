resource "aws_key_pair" "mein_key_pair" {
  public_key = file(var.key_path)
  key_name = var.key_name
}