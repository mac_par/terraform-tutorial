resource "aws_instance" "my_instance" {
  ami = var.image_id
  instance_type = var.instance_type
  subnet_id = aws_subnet.main_public_1.id
  vpc_security_group_ids = [aws_security_group.allow-ssh.id]
  user_data = data.template_cloudinit_config.user_data.rendered
  iam_instance_profile = aws_iam_instance_profile.s3-bucket-profile.name
  key_name = aws_key_pair.mein_key_pair.key_name
}