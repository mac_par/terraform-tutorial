terraform {
  required_version = ">= 0.12, < 0.13"
}
provider "aws" {
  region = var.region
}

resource "aws_instance" "my_instance" {
  ami = var.instance_image
  instance_type = "t2.micro"
}