variable "region" {
  type = string
  default = "eu-west-1"
}

variable "instance_image" {
  type = string
  default = "ami-04d5cc9b88f9d1d39"
  description = "proba-1"
}