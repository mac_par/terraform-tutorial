resource "aws_ecs_cluster" "myapp-cluster" {
  name = "myapp"
}

resource "aws_launch_configuration" "myapp-lauch_config" {
  name_prefix = "myapp-instance"
  image_id = var.ami_image
  instance_type = var.instance_type
  iam_instance_profile = aws_iam_instance_profile.ec2_instance_profile.id
  security_groups = [aws_security_group.instance-security-group.id]
  user_data            = "#!/bin/bash\necho 'ECS_CLUSTER=example-cluster' > /etc/ecs/ecs.config\nstart ecs"
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "myapp-autoscaling-grop" {
  name = "myapp-scaling-gropu"
  launch_configuration = aws_launch_configuration.myapp-lauch_config.name
  vpc_zone_identifier = [aws_subnet.main_public_1.id, aws_subnet.main_public_2.id]
  max_size = 1
  min_size = 1
  tag {
    key                 = "Name"
    value               = "ecs-ec2-container"
    propagate_at_launch = true
  }
}