terraform {
  required_version = ">= 0.12"
  backend "s3" {
    region = "eu-west-1"
    bucket = "terraform-state-mkbucket"
    key = "terraform/proba/tfstate.tf"
  }
}

provider "aws" {
  region = var.region
}