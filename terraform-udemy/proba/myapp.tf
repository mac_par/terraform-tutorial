data "template_file" "myapp-task-definition_template" {
  template = file(var.task-definion-template_path)
  vars = {
    REPOSITORY_URL = replace(aws_ecr_repository.myapp_repository.repository_url,"https://","")
  }
}

resource "aws_ecs_task_definition" "myapp-task-definition" {
  family = "myapp"
  container_definitions = data.template_file.myapp-task-definition_template.rendered
}

resource "aws_elb" "myapp-alb" {
  name = "myapp-alp-instance"
  connection_draining_timeout = 400
  connection_draining = true
  cross_zone_load_balancing = true
  idle_timeout = 400
  subnets = [aws_subnet.main_public_1.id, aws_subnet.main_public_2.id]
  security_groups = [aws_security_group.alb-security-group.id]

  listener {
    instance_port = var.ecs_port
    instance_protocol = "http"
    lb_port = var.app_port
    lb_protocol = "http"
  }

  health_check {
    healthy_threshold = 3
    interval = 60
    target = "http:${var.ecs_port}/"
    timeout = 30
    unhealthy_threshold = 3
  }
  tags = {
    Name = "myapp-alb"
  }
}

resource "aws_ecs_service" "myapp_ecs_service" {
  name = "myapp"
  cluster = aws_ecs_cluster.myapp-cluster.id
  task_definition = aws_ecs_task_definition.myapp-task-definition.arn
  desired_count = 1
  iam_role = aws_iam_role.ecs-assume-role.arn
  depends_on = [aws_iam_policy_attachment.ecs-policy-attachment]

  load_balancer {
    container_name = aws_ecs_cluster.myapp-cluster.name
    container_port = var.ecs_port
    elb_name = aws_elb.myapp-alb.name
  }

  lifecycle {
    ignore_changes = [task_definition]
  }
}