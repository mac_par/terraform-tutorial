variable "region" {
  type = string
  default = "eu-west-1"
}

variable "key_path" {
  type = string
  default = "keys/mykey.pub"
}

variable "ssh_port" {
  type = number
  default = 22
}

variable "app_port" {
  type = number
  default = 80
}

variable "ecs_port" {
  type = number
  default = 3000
}

variable "ami_image" {
  default = "ami-09266271a2521d06f"
  type = string
}

variable "instance_type" {
  type = string
  default = "t2.micro"
}

variable "task-definion-template_path" {
  type = string
  default = "templates/app.json.tpl"
}