resource "aws_security_group" "alb-security-group" {
  name = "alb-security-group"
  description = "security group used for alb"

  vpc_id = aws_vpc.my_vpc.id

  ingress {
    from_port = var.app_port
    protocol = "tcp"
    to_port = var.app_port
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "alb-security-group"
  }
}

resource "aws_security_group" "instance-security-group" {
  name = "ecs-instance-security-group"
  description = "security group used for ecs instance"

  vpc_id = aws_vpc.my_vpc.id

  ingress {
    from_port = var.ssh_port
    protocol = "tcp"
    to_port = var.ssh_port
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = var.ecs_port
    protocol = "tcp"
    to_port = var.ecs_port
    security_groups = [aws_security_group.alb-security-group.id]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "myapp-security-group"
  }
}