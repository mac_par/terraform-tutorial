resource "aws_key_pair" "my_key" {
  public_key = file(var.key_path)
  key_name = "my_key-pair"
}