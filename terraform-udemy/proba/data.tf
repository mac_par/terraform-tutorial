data "aws_iam_policy" "ecs-container-service-role" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceRole"
}