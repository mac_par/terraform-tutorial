variable "region" {
  type = string
  default = "eu-west-1"
}

variable "instance_image" {
  type = map(string)
  default = {
    eu-west-1 = "ami-04d5cc9b88f9d1d39"
  }
  description = "proba-1-instance"
}

variable "public_ssh_key_path" {
  type = string
  default = "mykey.pub"
}

variable "private_ssh_key_path" {
  type = string
  default = "mykey"
}

variable "user-key-name" {
  type = string
  default = "maciek@pc-5.home"
}