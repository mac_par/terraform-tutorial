resource "aws_key_pair" "my_key" {
  public_key = file(var.public_ssh_key_path)
  key_name = "my-key"
}

resource "aws_instance" "my_instance" {
  ami = lookup(var.instance_image, var.region)
  instance_type = "t2.micro"
  key_name = aws_key_pair.my_key.key_name

  provisioner "file" {
    destination = "/tmp/script.sh"
    source = "script.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/script.sh",
      "sudo /tmp/script.sh"
    ]
  }

  connection {
    user = var.user-key-name
    private_key = file(var.private_ssh_key_path)
    type = "ssh"
    host = coalesce(self.public_ip, self.private_ip)
  }

  tags = {
    dupa = "cos-tam"
    Name = "jakas-instancja-1"
  }
}