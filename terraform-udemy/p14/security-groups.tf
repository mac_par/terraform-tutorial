resource "aws_security_group" "allow-ssh" {
  name = "allow-ssh"
  description = "allows communication on ssh"
  vpc_id = aws_vpc.main_vpc.id
  ingress {
    from_port = var.ssh_port
    protocol = "tcp"
    to_port = var.ssh_port
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow-ssh-sg"
  }
}