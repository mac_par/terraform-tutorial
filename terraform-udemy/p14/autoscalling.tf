resource "aws_launch_configuration" "instance_launch_cfg" {
  image_id = var.ami_image
  instance_type = var.instance_type
  security_groups = [aws_security_group.allow-ssh.id]
  name_prefix = "example-instance"
  key_name = aws_key_pair.mein_key.key_name
  user_data = data.template_file.init_script.rendered
}

resource "aws_autoscaling_group" "example-scal-group" {
  name = "example-auto-scaling-group"
  min_size = 1
  max_size = 3
  desired_capacity = 1
  health_check_grace_period = 300
  launch_configuration = aws_launch_configuration.instance_launch_cfg.name
  vpc_zone_identifier = [aws_subnet.main_public_1.id, aws_subnet.main_public_2.id]
  health_check_type         = "EC2"
  force_delete              = true

  tag {
    key                 = "Name"
    value               = "ec2 instance"
    propagate_at_launch = true
  }
}

#autoscaling policy
resource "aws_autoscaling_policy" "scale_up" {
  autoscaling_group_name = aws_autoscaling_group.example-scal-group.name
  name = "example-cpu-up"
  adjustment_type = "ChangeInCapacity"
  cooldown = 300
  scaling_adjustment = 1
  policy_type = "SimpleScaling"
}

resource "aws_autoscaling_policy" "scale_down" {
  autoscaling_group_name = aws_autoscaling_group.example-scal-group.name
  name = "example-cpu-up"
  adjustment_type = "ChangeInCapacity"
  cooldown = 300
  scaling_adjustment = -1
  policy_type = "SimpleScaling"
}

#scale-up alarm
resource "aws_cloudwatch_metric_alarm" "scale_up_alarm" {
  alarm_name = "scale-up-now"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = 2
  threshold = 30
  period = 120
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  alarm_description = "jakis tam alarm przy powieksznu"
  statistic = "Average"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.example-scal-group.name
  }

  actions_enabled = true
  alarm_actions = [aws_autoscaling_policy.scale_up.arn]
}

resource "aws_cloudwatch_metric_alarm" "scale_down_alarm" {
  alarm_name = "scale-up-now"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = 2
  threshold = 5
  period = 120
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  alarm_description = "jakis tam alarm przy powieksznu"
  statistic = "Average"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.example-scal-group.name
  }

  actions_enabled = true
  alarm_actions = [aws_autoscaling_policy.scale_down.arn]
}