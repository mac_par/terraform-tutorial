variable "region" {
  type = string
  default = "eu-west-1"
}

variable "ami_image" {
  type = string
  default = "ami-035966e8adab4aaad"
}

variable "key_name" {
  type = string
  default = "mein_key"
}

variable "key_path" {
  type = string
  default = "keys/mykey.pub"
}

variable "instance_type" {
  type = string
  default = "t2.micro"
}

variable "ssh_port" {
  type = number
  default = 22
}

variable "init_script_path" {
  type = string
  default = "scripts/init.sh"
}