#!/bin/bash

sudo -s
apt-get upgrade
apt-get install -y mysql-client python-pip python-dev
apt-get update
pip install awscli