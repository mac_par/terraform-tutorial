resource "aws_key_pair" "my_pair" {
  public_key = file(var.key_path)
  key_name = "mykey_pair"
}

resource "aws_security_group" "allow_ssh_sg" {
  vpc_id = aws_vpc.my_vpc.id
  name = "allow-ssh-sq"
  description = "allows communication using ssh keys"
  ingress {
    from_port = var.ssh_port
    protocol = "tcp"
    to_port = var.ssh_port
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "my_instance" {
  ami = var.ami_id
  instance_type = var.instance_type
  key_name = aws_key_pair.my_pair.key_name
  vpc_security_group_ids = [aws_security_group.allow_ssh_sg.id]
  subnet_id = aws_subnet.main_public_1.id
  tags = {
    Name = "jakas instancja"
  }
}

resource "aws_ebs_volume" "additional_volumne" {
  availability_zone = "eu-west-1a"
  size = var.storage_size
  type = "gp2"
  tags = {
    Name = "dodatkowy wolumen"
  }
}

resource "aws_volume_attachment" "my_inst_volumne_1" {
  device_name = "/dev/xvdh"
  instance_id = aws_instance.my_instance.id
  volume_id = aws_ebs_volume.additional_volumne.id
}