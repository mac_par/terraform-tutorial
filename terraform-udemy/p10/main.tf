terraform {
  required_version = ">= 0.12, < 0.13"
  backend "s3" {
    bucket = "terraform-state-mkbucket"
    key = "terraform/p10/terraform.tfstate"
    region = "eu-west-1"
  }
}

provider "aws" {
  region = var.region
}