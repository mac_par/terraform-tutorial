variable "region" {
  type = string
  default = "eu-west-1"
}

variable "instance_type" {
  type = string
  default = "t2.micro"
}

variable "ami_id" {
  type = string
  default = "ami-04d5cc9b88f9d1d39"
}

variable "storage_size" {
  type = number
  default = 8
}

variable "ssh_port" {
  type = number
  default = 22
}

variable "key_path" {
  type = string
  default = "mykey.pub"
}