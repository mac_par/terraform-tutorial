resource "aws_vpc" "my_vpc" {
  cidr_block = "10.0.0.0/16"
  enable_classiclink = false
  enable_dns_hostnames = true
  enable_dns_support = true
  instance_tenancy = "default"

  tags = {
    Name = "my_main_vpc"
  }
}

#create subnets
##public
resource "aws_subnet" "main_public_1" {
  cidr_block = "10.0.1.0/24"
  vpc_id = aws_vpc.my_vpc.id
  availability_zone = "eu-west-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "main-public-1"
  }
}

resource "aws_subnet" "main_public_2" {
  cidr_block = "10.0.2.0/24"
  vpc_id = aws_vpc.my_vpc.id
  availability_zone = "eu-west-1b"
  map_public_ip_on_launch = true

  tags = {
    Name = "main-public-2"
  }
}

resource "aws_subnet" "main_public_3" {
  cidr_block = "10.0.3.0/24"
  vpc_id = aws_vpc.my_vpc.id
  availability_zone = "eu-west-1c"
  map_public_ip_on_launch = true

  tags = {
    Name = "main-public-3"
  }
}

#private

resource "aws_subnet" "main_private_1" {
  cidr_block = "10.0.4.0/24"
  vpc_id = aws_vpc.my_vpc.id
  availability_zone = "eu-west-1a"
  map_public_ip_on_launch = true

  tags = {
    Name = "main-private-1"
  }
}
resource "aws_subnet" "main_private_2" {
  cidr_block = "10.0.5.0/24"
  vpc_id = aws_vpc.my_vpc.id
  availability_zone = "eu-west-1b"
  map_public_ip_on_launch = true

  tags = {
    Name = "main-private-2"
  }
}
resource "aws_subnet" "main_private_3" {
  cidr_block = "10.0.6.0/24"
  vpc_id = aws_vpc.my_vpc.id
  availability_zone = "eu-west-1c"
  map_public_ip_on_launch = true

  tags = {
    Name = "main-private-3"
  }
}

resource "aws_internet_gateway" "main_inter_gateway" {
  vpc_id = aws_vpc.my_vpc.id
  tags = {
    Name = "main_gateway"
  }
}

resource "aws_route_table" "my_public_route_table" {
  vpc_id = aws_vpc.my_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main_inter_gateway.id
  }

  tags = {
    Name = "main_route_table"
  }
}

#public route table associations
resource "aws_route_table_association" "main_public_1_a" {
  route_table_id = aws_route_table.my_public_route_table.id
  subnet_id = aws_subnet.main_public_1.id
}

resource "aws_route_table_association" "main_public_2_a" {
  route_table_id = aws_route_table.my_public_route_table.id
  subnet_id = aws_subnet.main_public_2.id
}

resource "aws_route_table_association" "main_public_3_a" {
  route_table_id = aws_route_table.my_public_route_table.id
  subnet_id = aws_subnet.main_public_3.id
}

#nat for private subnets
resource "aws_eip" "nat" {
  vpc = true
}

#nat gateway
resource "aws_nat_gateway" "nat_private_gateway" {
  allocation_id = aws_eip.nat.id
  subnet_id = aws_subnet.main_public_1.id
  depends_on = [aws_internet_gateway.main_inter_gateway]
}

resource "aws_route_table" "main_private_route_table" {
  vpc_id = aws_vpc.my_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.nat_private_gateway.id
  }

  tags = {
    Name = "main_private_route_table"
  }
}

#route table associations
resource "aws_route_table_association" "main_private_1_a" {
  subnet_id = aws_subnet.main_private_1.id
  route_table_id = aws_route_table.main_private_route_table.id
}

resource "aws_route_table_association" "main_private_2_a" {
  subnet_id = aws_subnet.main_private_2.id
  route_table_id = aws_route_table.main_private_route_table.id
}

resource "aws_route_table_association" "main_private_3_a" {
  subnet_id = aws_subnet.main_private_3.id
  route_table_id = aws_route_table.main_private_route_table.id
}
